# Depot Composition
This repo provides a cli for analyzing portfolio performance xml files (https://www.portfolio-performance.info) and adds classification information like countries or shares into the save file.

## Usage
You need to have golang installed to compile a version of the tool.
* clone this project into your go workspace
* compile the software: `make build`
* move the binary (linux) or .exe (windows) to you portfolio performance save file
* run the program and name the save file as the first parameter: `./depot-composition testdepot.xml`
    * the tool will create a backup file of the xml-file before changing anything
    * any change will directly applied to the original file
    
## How it works
Portfolio performance saves any information about your depots inside an xml file. The tool provided in this repo adds classification about countries, industries, instruments and shares of fonds like etfs by following these steps:
1. read in and parse the important parts of the xml: securities and taxonomies
2. isins from securities are used to fetch classification information from onvista.de by using the same api endpoint onvista is using in their web frontend
    * example with curl: 
      ```
      curl --request POST \
          --url https://www.onvista.de/etf/ajax/etfBreakdownList \
          --header 'Content-Type: multipart/form-data' \
          --header 'content-type: multipart/form-data; boundary=---011000010111000001101001' \
          --form isin=DE0009848119 \
          --form type=countries
      ```
3. since share data is not included in the endpoint the page `https://www.onvista.de/etf/anlageschwerpunkt/DE0009848119` is parsed to fetch the necessary information
4. all data is filtered, clustered and colorized
5. new classifications are added to the xml file (if classifications are already exist, they are replaced)

## Disclaimer
This software is provided "as it". There is no warranty or anything similar when downloading or using the software.

Please also notice that this repo contains several hard-coded parts which are specified for the german language (especially labels) and might not work in future.