package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

type CacheEntry struct {
	CreatedAt time.Time
	Data      string
}

type cacheMap map[string]CacheEntry

var cache cacheMap

func init() {
	Load()
}

func NewCacheEntry(data string) CacheEntry {
	return CacheEntry{
		CreatedAt: time.Now(),
		Data:      data,
	}
}

func HasEntry(key string) bool {
	_, exist := cache[key]
	return exist
}

func GetEntry(key string) (CacheEntry, bool) {
	entry, exist := cache[key]
	return entry, exist
}

func SetEntry(key string, entry CacheEntry) {
	cache[key] = entry
}

func Save() {
	data, _ := json.Marshal(cache)
	f, _ := os.Create("requests.json")

	fmt.Fprint(f, string(data))
}

func Load() {
	cache = cacheMap{}
	f, err := os.Open("requests.json")

	if err != nil {
		return
	}

	data, _ := ioutil.ReadAll(f)

	cp := &cache
	if err := json.Unmarshal(data, cp); err != nil {
		cache = *cp
	}
}

func Get(url string) string {
	if HasEntry(url) {
		entry, _ := GetEntry(url)
		return entry.Data
	} else {
		defer Save()
	}

	resp, _ := http.Get(url)

	bb, _ := ioutil.ReadAll(resp.Body)
	sb := string(bb)

	SetEntry(url, NewCacheEntry(sb))
	return sb
}
