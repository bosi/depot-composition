package main

import (
	"github.com/gerow/go-color"
)

func genColors(num int) []string {
	start := 40.0
	end := 360.0

	step := (end - start) / float64(num)

	colors := []string{}

	for current := start; current <= end; current += step {
		hsl := color.HSL{
			H: current / 360,
			S: 0.9,
			L: 0.5,
		}

		colors = append(colors, "#"+hsl.ToHTML())
	}

	return colors
}
