package main

import (
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("you have to provide the filename of your portfolio xml as the first cli argument")
	}

	portfolioFilename := os.Args[1]

	p := loadPortfolio(portfolioFilename)
	p.apply(p.assetData())

	xmlBytes, _ := xml.Marshal(p.Taxonomies.Taxonomy)
	xmlString := string(xmlBytes)
	newTaxonomies := fmt.Sprintf("<taxonomies>%s</taxonomies>", xmlString)
	re := regexp.MustCompile(`(?is)<taxonomies>.*</taxonomies>`)

	portfolioConfig, _ := os.Open(portfolioFilename)
	bakFile, _ := os.Create(fmt.Sprintf(
		"%s_bak_%d.xml",
		strings.Split(portfolioFilename, ".")[0],
		time.Now().Unix(),
	))
	io.Copy(bakFile, portfolioConfig)
	portfolioConfig.Seek(0, 0)
	oldXml, _ := ioutil.ReadAll(portfolioConfig)

	newXml := re.ReplaceAllString(string(oldXml), newTaxonomies)
	portfolioConfig, _ = os.Create(portfolioFilename)
	fmt.Fprint(portfolioConfig, newXml)
}
