
dep:
	go get -v ./...

run: dep
	go run *.go

build: dep
	GOOS=linux GOARCH=386 go build -o depot-composition
	GOOS=windows GOARCH=386 go build -o depot-composition.exe