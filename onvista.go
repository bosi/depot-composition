package main

import (
	"encoding/json"
	"github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type FundamentalData struct {
	ISIN        string
	Industries  []InvestmentFocusPart
	Currencies  []InvestmentFocusPart
	Instruments []InvestmentFocusPart
	Countries   []InvestmentFocusPart
	Shares      []InvestmentFocusPart
}

type InvestmentFocusPart struct {
	Name       string
	Percentage float64
}

func dePercentageToFloat(in string) float64 {
	in = strings.Trim(in, "%")
	in = strings.ReplaceAll(in, ",", ".")
	perc, _ := strconv.ParseFloat(in, 64)
	return perc
}

func fetchFundamentalData(isin string) FundamentalData {
	fd := FundamentalData{
		ISIN:        isin,
		Industries:  filter(fetchInvestmentFocusParts(isin, "industries"), industriesMap),
		Currencies:  filter(fetchInvestmentFocusParts(isin, "currencies"), currenciesMap),
		Instruments: filter(fetchInvestmentFocusParts(isin, "instruments"), instrumentsMap),
		Countries:   filter(fetchInvestmentFocusParts(isin, "countries"), countryNameReplacements),
		Shares:      filter(fetchFondShares(isin), sharesMap),
	}

	return fd
}

func filter(ifps []InvestmentFocusPart, filterMap map[string]string) []InvestmentFocusPart {
	ret := []InvestmentFocusPart{}

	for _, ifp := range ifps {
		target, ok := filterMap[ifp.Name]

		if ok {
			if target == "" {
				continue
			} else {
				ifp.Name = target
			}
		}

		func() {
			for i, part := range ret {
				if part.Name == ifp.Name {
					ret[i].Percentage += ifp.Percentage
					return
				}
			}
			ret = append(ret, ifp)
		}()
	}

	rest := 100.0
	for _, part := range ret {
		rest -= part.Percentage
	}

	if rest > 0 {
		ret = append(ret, InvestmentFocusPart{
			Name:       "Andere",
			Percentage: rest,
		})
	}

	return ret
}

func fetchFondShares(isin string) []InvestmentFocusPart {
	res := Get("https://www.onvista.de/etf/anlageschwerpunkt/" + isin)
	shares := []InvestmentFocusPart{}

	doc, _ := goquery.NewDocumentFromReader(strings.NewReader(res))
	doc.Find("#etf-snapshot h2").Each(func(i int, selection *goquery.Selection) {
		if strings.Contains(selection.Text(), "Top Holdings") {
			selection.Parent().Find("table tbody tr").Each(func(i int, selection *goquery.Selection) {
				tds := selection.Children()
				holding := strings.Trim(tds.Eq(1).Text(), " \t\n")
				percentage := strings.Trim(tds.Eq(0).Text(), " \t\n")

				shares = append(shares, InvestmentFocusPart{
					Name:       holding,
					Percentage: dePercentageToFloat(percentage),
				})
			})
		}
	})

	return shares
}

func fetchInvestmentFocusParts(isin string, focusType string) []InvestmentFocusPart {
	resp, _ := http.PostForm("https://www.onvista.de/etf/ajax/etfBreakdownList", map[string][]string{
		"isin": {isin},
		"type": {focusType},
	})
	d, _ := ioutil.ReadAll(resp.Body)
	parsedData := [][]interface{}{}
	json.Unmarshal(d, &parsedData)

	ifps := []InvestmentFocusPart{}
	for _, inItem := range parsedData {
		ifps = append(ifps, InvestmentFocusPart{
			Name:       inItem[0].(string),
			Percentage: inItem[1].(float64),
		})
	}

	return ifps
}
