package main

import (
	"encoding/xml"
	"github.com/google/uuid"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
)

type Client struct {
	XMLName    xml.Name   `xml:"client"`
	Securities Securities `xml:"securities"`
	Taxonomies Taxonomies `xml:"taxonomies"`
}

type Securities struct {
	XMLName  xml.Name   `xml:"securities"`
	Security []Security `xml:"security"`
}

type Security struct {
	XMLName xml.Name `xml:"security"`
	UUID    string   `xml:"uuid"`
	ISIN    string   `xml:"isin"`
	Name    string   `xml:"name"`
}

type Taxonomies struct {
	XMLName  xml.Name   `xml:"taxonomies"`
	Taxonomy []Taxonomy `xml:"taxonomy"`
}

type Taxonomy struct {
	XMLName xml.Name `xml:"taxonomy"`
	ID      string   `xml:"id"`
	Name    string   `xml:"name"`
	Root    Root     `xml:"root"`
}

type Root struct {
	XMLName     xml.Name    `xml:"root"`
	ID          string      `xml:"id"`
	Name        string      `xml:"name"`
	Color       string      `xml:"color"`
	Children    Children    `xml:"children"`
	Assignments Assignments `xml:"assignments"`
	Weight      int         `xml:"weight"`
	Rank        int         `xml:"rank"`
}

type Children struct {
	XMLName        xml.Name         `xml:"children"`
	Classification []Classification `xml:"classification"`
}

type Classification struct {
	XMLName xml.Name `xml:"classification"`
	Parent  Parent   `xml:"parent"`
	Root
}

type Parent struct {
	XMLName   xml.Name `xml:"parent"`
	Reference string   `xml:"reference,attr"`
}

type Assignments struct {
	XMLName    xml.Name     `xml:"assignments"`
	Assignment []Assignment `xml:"assignment"`
}

type Assignment struct {
	XMLName           xml.Name          `xml:"assignment"`
	InvestmentVehicle InvestmentVehicle `xml:"investmentVehicle"`
	Weight            int               `xml:"weight"`
	Rank              int               `xml:"rank"`
}

type InvestmentVehicle struct {
	XMLName   xml.Name `xml:"investmentVehicle"`
	Class     string   `xml:"class,attr"`
	Reference string   `xml:"reference,attr"`
}

func loadPortfolio(filename string) Client {
	xmlFile, _ := os.Open(filename)
	xmlBytes, _ := ioutil.ReadAll(xmlFile)

	c := Client{}
	xml.Unmarshal(xmlBytes, &c)

	return c
}

func (c Client) assetData() []FundamentalData {
	assetData := []FundamentalData{}
	for _, asset := range c.Securities.Security {
		if len(asset.ISIN) == 0 {
			continue
		}
		assetData = append(assetData, fetchFundamentalData(asset.ISIN))
	}

	return assetData
}

func newRoot(name string) Root {
	return Root{
		ID:          uuid.New().String(),
		Name:        name,
		Color:       "#dddddd",
		Assignments: Assignments{Assignment: []Assignment{}},
		Weight:      10000,
		Rank:        0,
		Children:    Children{},
	}
}

func newClassification(name string) Classification {
	return Classification{
		Parent: Parent{Reference: "../../.."},
		Root:   newRoot(name),
	}
}

func newAssignment(securityIndex int, percentage float64) Assignment {
	return Assignment{
		InvestmentVehicle: InvestmentVehicle{
			Class:     "security",
			Reference: "../../../../../../../../securities/security[" + strconv.Itoa(securityIndex) + "]",
		},
		Weight: int(percentage * 100),
		Rank:   0,
	}
}

func (c *Client) apply(fd []FundamentalData) {
	taxes := map[string]Taxonomy{
		"industries":  {ID: uuid.New().String(), Name: "Branchen (auto-generated)", Root: newRoot("Branchen (auto-generated)")},
		"currencies":  {ID: uuid.New().String(), Name: "Währungen (auto-generated)", Root: newRoot("Währungen (auto-generated)")},
		"instruments": {ID: uuid.New().String(), Name: "Instrumente (auto-generated)", Root: newRoot("Instrumente (auto-generated)")},
		"countries":   {ID: uuid.New().String(), Name: "Länder (auto-generated)", Root: newRoot("Länder (auto-generated)")},
		"shares":      {ID: uuid.New().String(), Name: "Holdings (auto-generated)", Root: newRoot("Holdings (auto-generated)")},
	}

	taxesItems := map[string]*Children{}

	for key, _ := range taxes {
		taxesItems[key] = &Children{}
	}

	for _, assetData := range fd {
		assetDatas := map[string][]InvestmentFocusPart{
			"industries":  assetData.Industries,
			"currencies":  assetData.Currencies,
			"instruments": assetData.Instruments,
			"countries":   assetData.Countries,
			"shares":      assetData.Shares,
		}

		for key, parts := range assetDatas {
			for _, part := range parts {
				as := newAssignment(c.securityIndexByISIN(assetData.ISIN), part.Percentage)
				taxesItems[key].ensureClassification(part.Name)
				taxesItems[key].add(part.Name, as)
			}
		}
	}

	taxCountries := Children{Classification: []Classification{}}

	for _, taxesItem := range taxesItems["countries"].Classification {
		if groupName, ok := countryMap[taxesItem.Name]; ok {
			taxCountries.ensureClassification(groupName)
			taxCountries.Classification[taxCountries.classificationIDByName(groupName)].addSubClass(taxesItem)
		} else {
			taxCountries.Classification = append(taxCountries.Classification, taxesItem)
		}
	}
	taxesItems["countries"] = &taxCountries

	for key, taxonomy := range taxes {
		taxonomy.Root.Children = *taxesItems[key]
		taxonomy.Root.Children.sort()
		taxonomy.Root.Children.generateColors()

		c.Taxonomies.addOrReplace(taxonomy)
	}
}

func (cl *Classification) addSubClass(subCl Classification) {
	for i, _ := range subCl.Assignments.Assignment {
		subCl.Assignments.Assignment[i].InvestmentVehicle.Reference = "../../" + subCl.Assignments.Assignment[i].InvestmentVehicle.Reference
	}

	cl.Children.Classification = append(cl.Children.Classification, subCl)
}

func (taxs *Taxonomies) addOrReplace(newTax Taxonomy) {
	for i, tax := range taxs.Taxonomy {
		if tax.Name == newTax.Name {
			taxs.Taxonomy[i] = newTax
			return
		}
	}

	taxs.Taxonomy = append(taxs.Taxonomy, newTax)
}

func (c Client) securityIndexByISIN(isin string) int {
	for i, sec := range c.Securities.Security {
		if sec.ISIN == isin {
			return i + 1
		}
	}

	return 0
}

func (children Children) classificationIDByName(name string) int {
	for i, classification := range children.Classification {
		if classification.Name == name {
			return i
		}
	}

	return 0
}

func (children *Children) sort() {
	sort.Slice(children.Classification, func(i, j int) bool {
		if children.Classification[j].Name == "Andere" {
			return true
		} else if children.Classification[i].Name == "Andere" {
			return false
		}

		return children.Classification[i].Name < children.Classification[j].Name
	})

	for _, classification := range children.Classification {
		classification.Children.sort()
	}
}

func (children *Children) generateColors() {
	colors := genColors(children.numOfClassifications())

	offset := 0
	for i, _ := range children.Classification {
		children.Classification[i].Color = colors[i+offset]

		for j, _ := range children.Classification[i].Children.Classification {
			offset++
			children.Classification[i].Children.Classification[j].Color = colors[i+offset]
		}
	}
}

func (children Children) numOfClassifications() int {
	num := 0
	for _, cl := range children.Classification {
		num++
		if len(cl.Children.Classification) > 0 {
			num += cl.Children.numOfClassifications()
		}
	}

	return num
}

func (children *Children) ensureClassification(name string) {
	if !children.exists(name) {
		children.Classification = append(
			children.Classification,
			newClassification(name),
		)
	}
}

func (children *Children) get(name string) *Classification {
	for _, cl := range children.Classification {
		if cl.Name == name {
			return &cl
		}
	}

	return &Classification{}
}

func (children *Children) add(name string, as Assignment) {
	for i, cl := range children.Classification {
		if cl.Name == name {
			children.Classification[i].Assignments.Assignment = append(children.Classification[i].Assignments.Assignment, as)
			return
		}
	}
}

func (children Children) exists(name string) bool {
	for _, cl := range children.Classification {
		if cl.Name == name {
			return true
		}
	}

	return false
}
